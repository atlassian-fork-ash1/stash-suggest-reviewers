package com.atlassian.bitbucket.server.suggestreviewers.internal;

import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitRequest;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.CommandFailedException;
import com.atlassian.bitbucket.server.suggestreviewers.internal.suggester.git.FirstLineOutputHandler;

/**
 * Determines the merge base of a pair of commits.
 */
public class MergeBaseResolver {

    private final SuggestionCommandBuilderFactory builderFactory;
    private final CommitService commitService;

    public MergeBaseResolver(SuggestionCommandBuilderFactory builderFactory, CommitService commitService) {
        this.builderFactory = builderFactory;
        this.commitService = commitService;
    }

    public Commit findMergeBase(Commit a, Commit b) {
        Repository repo = a.getRepository();
        if (repo == null) {
            return null;
        }
        try {
            String sha = builderFactory.builderFor(a, b)
                    .mergeBase()
                    .between(a.getId(), b.getId())
                    .build(new FirstLineOutputHandler())
                    .call();
            if (sha == null) {
                return null;
            }
            return commitService.getCommit(new CommitRequest.Builder(repo, sha).build());
        } catch (CommandFailedException e) {
            return null;
        }
    }
}
