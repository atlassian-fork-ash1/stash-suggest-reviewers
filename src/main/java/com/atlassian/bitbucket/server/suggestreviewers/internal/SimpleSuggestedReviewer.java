package com.atlassian.bitbucket.server.suggestreviewers.internal;

import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.server.suggestreviewers.SuggestedReviewer;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

public class SimpleSuggestedReviewer implements SuggestedReviewer {

    private final ApplicationUser user;
    private final String shortReason;
    private final Iterable<String> reasons;
    private final Integer reviewingCount;

    public SimpleSuggestedReviewer(@Nonnull ApplicationUser user, @Nonnull String shortReason,
                                   @Nonnull Iterable<String> reasons, @Nonnull Integer reviewingCount) {
        this.user = requireNonNull(user, "user");
        this.shortReason = requireNonNull(shortReason, "shortReason");
        this.reasons = requireNonNull(reasons, "reasons");
        this.reviewingCount = requireNonNull(reviewingCount, "reviewingCount");
    }

    @Nonnull
    @Override
    public ApplicationUser getUser() {
        return user;
    }

    @Nonnull
    @Override
    public String getShortReason() {
        return shortReason;
    }

    @Nonnull
    @Override
    public Iterable<String> getReasons() {
        return reasons;
    }

    @Nonnull
    @Override
    public Integer getReviewingCount() {
        return reviewingCount;
    }
}
