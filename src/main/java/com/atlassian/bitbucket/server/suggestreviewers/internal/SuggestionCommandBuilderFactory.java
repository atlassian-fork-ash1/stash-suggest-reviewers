package com.atlassian.bitbucket.server.suggestreviewers.internal;

import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.git.command.GitScmCommandBuilder;

import javax.annotation.Nonnull;

/**
 * An abstraction over creating {@link GitScmCommandBuilder}s.
 *
 * @since 3.1.1
 */
public interface SuggestionCommandBuilderFactory {

    /**
     * @param since the since commit
     * @param until the until commit
     * @return a builder which has been prepared for accessing objects in both the {@code since} and {@code until}
     *         commits' repositories (if they're not in the same repository)
     */
    @Nonnull
    GitScmCommandBuilder builderFor(@Nonnull Commit since, @Nonnull Commit until);

    /**
     * @param repository the primary repository
     * @param secondary  the secondary repository, which may be the same as {@code primary} but not {@code null}
     * @return a builder which has been prepared for accessing objects in both repositories, if they're not the same
     */
    @Nonnull
    GitScmCommandBuilder builderFor(@Nonnull Repository repository, @Nonnull Repository secondary);
}
