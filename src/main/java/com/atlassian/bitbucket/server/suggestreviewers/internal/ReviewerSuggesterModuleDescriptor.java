package com.atlassian.bitbucket.server.suggestreviewers.internal;

import com.atlassian.bitbucket.server.suggestreviewers.spi.ReviewerSuggester;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationPattern;

import static com.atlassian.plugin.util.validation.ValidationPattern.test;

public class ReviewerSuggesterModuleDescriptor extends AbstractModuleDescriptor<ReviewerSuggester> {

    public static final String XML_ELEMENT_NAME = "reviewer-suggester";

    public ReviewerSuggesterModuleDescriptor(ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    protected void provideValidationRules(ValidationPattern pattern) {
        super.provideValidationRules(pattern);

        pattern.rule(test("@class")
                .withError("The " + ReviewerSuggester.class.getSimpleName() + " class attribute is required."));
    }

    @Override
    public ReviewerSuggester getModule() {
        return moduleFactory.createModule(moduleClassName, this);
    }
}
